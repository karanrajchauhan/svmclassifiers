tic
% Format is DocID, WordID, WordCount

%% LOAD DATA

% directory where all the data is stored
data_dir = './files_for_assignment6/';

% load train data
fileID = fopen(strcat(data_dir, 'train.data'));
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

% load test data
fileID = fopen(strcat(data_dir, 'test.data'));
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

% load train labels
fileID = fopen(strcat(data_dir, 'train.label'));
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

% load test labels
fileID = fopen(strcat(data_dir, 'test.label'));
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

% load vocab
vocab = importdata(strcat(data_dir, 'vocabulary.txt'));

% load stopwords
stopwords = importdata(strcat(data_dir, 'stoplist.txt'));


%% REMOVE STOP WORDS

% get word id corresponding to each stop word
[~, stop_word_ids] = ismember(stopwords, vocab);

% do not consider words that are in stoplist but not vocab
stop_word_ids = unique(stop_word_ids(stop_word_ids~=0));

% remove stop words from train data
keep_idx = ~ismember(X_train(:,2), stop_word_ids);
X_train = X_train(keep_idx, :);

% remove stop words from test data
keep_idx = ~ismember(X_test(:,2), stop_word_ids);
X_test = X_test(keep_idx, :);


%% KEEP DATA CORRESPONDING TO CLASS 1 AND 20

% find indices where class label is 1 or 20
train_1_20_idx = y_train == 1 | y_train == 20;
test_1_20_idx = y_test == 1 | y_test == 20;

% get doc ids of docs that belong to class 1 or 20
train_doc_ids = find(train_1_20_idx);
test_doc_ids = find(test_1_20_idx);

% keep X corresponding to class labels 1 and 20 only
X_train = X_train(ismember(X_train(:,1), train_doc_ids), :);
X_test = X_test(ismember(X_test(:,1), test_doc_ids), :);

% keep y corresponding to class labels 1 and 20 only
y_train = y_train(train_1_20_idx, :);
y_test = y_test(test_1_20_idx, :);


%% CREATE "WORD-COUNT IN DOCUMENT" MATRIX

% initialize for use inside for loops
doc_indices = zeros(size(X_train, 1), 1);

% jth col of this matrix is word count vector for doc id j
words_in_train_docs = zeros(numel(vocab), numel(train_doc_ids));

% create the matrix for train data
for doc_id=1:numel(train_doc_ids)
    
    % indices where doc id is same is current doc_id
    doc_indices = X_train(:,1) == doc_id;
    
    % populate the matrix with the data for current doc_id
    words_in_train_docs(X_train(doc_indices, 2), doc_id) = X_train(doc_indices, 3);

end

% jth col of this matrix is word count vector for doc id j
words_in_test_docs = zeros(numel(vocab), numel(test_doc_ids));

% create the matrix for train data
for doc_id=1:numel(test_doc_ids)
    
    % indices where doc id is same is current doc_id
    doc_indices = X_test(:,1) == doc_id;
    
    % populate the matrix with the data for current doc_id
    words_in_test_docs(X_test(doc_indices, 2), doc_id) = X_test(doc_indices, 3);

end


%% PARTITION DATA INTO 5 SETS AND CROSS VALIDATE

X_train_cv = cvpartition(size(words_in_train_docs,2), 'KFold', 5);

% values of c to try in cross validation
c_vals = -7:13;
c_vals = 2 .^ c_vals;

% values of sigma to try in cross validation
sigma_vals = -7:9;
sigma_vals = 2 .^ sigma_vals;

% average ccr for each value of c
ccrs_for_c_sigma = zeros(numel(c_vals), numel(sigma_vals));

% ccrs obtained in each fold of cross validation
folds_ccrs = zeros(5,1);

% initialize predictions and correct(logical) arrays in each fold
fold_preds = zeros(max(X_train_cv.TestSize));
fold_correct = zeros(max(X_train_cv.TestSize));

for c_idx = 1:numel(c_vals)
    
    % current value of c
    c = c_vals(c_idx);
    
    for sigma_idx = 1:numel(sigma_vals)
        
        % current value of sigma
        sigma = sigma_vals(sigma_idx);
        
        % 5 fold cross validatation to find ccr
        for fold_num = 1:X_train_cv.NumTestSets

            % number of training points
            fold_num_train = sum(X_train_cv.training(fold_num));
            
            % train svm on current training set
            fold_svm_struct = svmtrain(words_in_train_docs(:, X_train_cv.training(fold_num))', ...
                                y_train(X_train_cv.training(fold_num)), ...
                                'autoscale', 'false', ...
                                'boxconstraint', c*ones(fold_num_train,1), ...
                                'kernel_function', 'rbf', ...
                                'rbf_sigma', sigma);

            % classifier predictions 
            fold_preds = svmclassify(fold_svm_struct, words_in_train_docs(:, X_train_cv.test(fold_num))');

            % logical array of whether predicted label was same as truth label
            fold_correct = (fold_preds==y_train(X_train_cv.test(fold_num)));

            % add current ccr to folds ccrs array
            folds_ccrs(fold_num, 1) = sum(fold_correct)/numel(fold_correct);

        end
        
        % save average of ccrs from 5 folds as ccr for current c - sigma combination
        ccrs_for_c_sigma(c_idx, sigma_idx) = mean(folds_ccrs);       
    
    end
    
end

% save the ccr matrix for possible future usages
save('C_Sigma_CCRvals2.mat', 'ccrs_for_c_sigma');

%% PLOT CV - CCR

figure;
[c, h] = contour(log2(c_vals), log2(sigma_vals), ccrs_for_c_sigma'), clabel(c,h);
colorbar
xlabel('Regularization parameter in log scale (base 2)');
ylabel('RBF-sigma in log scale (base 2)');


%% OPTIMAL VALUE OF C AND SIGMA

% linear index of max ccr
[~, best_ccr_idx] = max(ccrs_for_c_sigma(:));

% row and column index of ccrs_for_c_sigma where max ccr occurs
[best_c_idx, best_sigma_idx] = ind2sub(size(ccrs_for_c_sigma), best_ccr_idx);

% value of c for which max ccr occurs
best_c = c_vals(best_c_idx);

% value of sigma for which max ccr occurs
best_sigma = sigma_vals(best_sigma_idx);


%% TRAIN ON ALL TRAIN DATA AND TEST ON ALL TEST DATA FOR BEST C, SIGMA

% train svm on current training set
svm_struct = svmtrain(words_in_train_docs', y_train, ...
                    'autoscale', 'false', ...
                    'boxconstraint', best_c*ones(numel(y_train),1), ...
                    'kernel_function', 'rbf', ...
                    'rbf_sigma', best_sigma);
                
% classifier predictions 
preds = svmclassify(svm_struct, words_in_test_docs');

% logical array of whether predicted label was same as truth label
correct = (preds==y_test);

% get ccr
ccr = sum(correct)/numel(correct);

toc