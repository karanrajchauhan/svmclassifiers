tic
% Format is DocID, WordID, WordCount

%% LOAD DATA

% directory where all the data is stored
data_dir = './files_for_assignment6/';

% load train data
fileID = fopen(strcat(data_dir, 'train.data'));
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

% load test data
fileID = fopen(strcat(data_dir, 'test.data'));
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

% load train labels
fileID = fopen(strcat(data_dir, 'train.label'));
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

% load test labels
fileID = fopen(strcat(data_dir, 'test.label'));
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

% load vocab
vocab = importdata(strcat(data_dir, 'vocabulary.txt'));

% load stopwords
stopwords = importdata(strcat(data_dir, 'stoplist.txt'));


%% REMOVE STOP WORDS

% get word id corresponding to each stop word
[~, stop_word_ids] = ismember(stopwords, vocab);

% do not consider words that are in stoplist but not vocab
stop_word_ids = unique(stop_word_ids(stop_word_ids~=0));

% remove stop words from train data
keep_idx = ~ismember(X_train(:,2), stop_word_ids);
X_train = X_train(keep_idx, :);

% remove stop words from test data
keep_idx = ~ismember(X_test(:,2), stop_word_ids);
X_test = X_test(keep_idx, :);


%% CREATE "WORD-COUNT IN DOCUMENT" MATRIX

% relabel class 7 as 1 and all others as 0
y_train = (y_train == 7);
y_test = (y_test == 7);

% initialize for use inside for loops
doc_indices = zeros(size(X_train, 1), 1);

% jth col of this matrix is word count vector for doc id j
words_in_train_docs = zeros(numel(vocab), numel(y_train));

% create the matrix for train data
for doc_id=1:numel(y_train)
    
    % indices where doc id is same is current doc_id
    doc_indices = X_train(:,1) == doc_id;
    
    % populate the matrix with the data for current doc_id
    words_in_train_docs(X_train(doc_indices, 2), doc_id) = X_train(doc_indices, 3);

end

% jth col of this matrix is word count vector for doc id j
words_in_test_docs = zeros(numel(vocab), numel(y_test));

% create the matrix for train data
for doc_id=1:numel(y_test)
    
    % indices where doc id is same is current doc_id
    doc_indices = X_test(:,1) == doc_id;
    
    % populate the matrix with the data for current doc_id
    words_in_test_docs(X_test(doc_indices, 2), doc_id) = X_test(doc_indices, 3);

end


%% PARTITION DATA INTO 5 SETS AND CROSS VALIDATE

% partitions for 5 fold cv
X_partitions = cvpartition(y_train, 'Kfold', 5);

% values of c to try in cross validation
c_vals = -9:10;
c_vals = 2 .^ c_vals;

% average ccr, precision, recall, f1 score for each value of c
ccrs_for_c = zeros(numel(c_vals),1);
precisions_for_c = zeros(numel(c_vals),1);
recalls_for_c = zeros(numel(c_vals),1);
f1s_for_c = zeros(numel(c_vals),1);

% ccrs, precisions, recalls, f1 scores obtained in each fold of cross validation
folds_ccrs = zeros(5,1);
folds_precisions = zeros(5,1);
folds_recalls = zeros(5,1);
folds_f1s = zeros(5,1);

% initialize predictions and correct(logical) arrays in each fold
fold_preds = zeros(max(X_partitions.TestSize));
fold_correct = zeros(max(X_partitions.TestSize));

for c_idx = 1:numel(c_vals)
    
    % the value of c to try
    c = c_vals(c_idx);
    
    % 5 fold cross validatation to find ccr
    for fold_num = 1:X_partitions.NumTestSets

        % number of training points
        fold_num_train = sum(X_partitions.training(fold_num));
        
        % train svm on current training set
        fold_svm_struct = svmtrain(words_in_train_docs(:, X_partitions.training(fold_num))', ...
                            y_train(X_partitions.training(fold_num)), ...
                            'autoscale', 'false', ...
                            'boxconstraint', c*ones(fold_num_train,1), ...
                            'kernel_function', 'linear');
                     
        % classifier predictions 
        fold_preds = svmclassify(fold_svm_struct, words_in_train_docs(:, X_partitions.test(fold_num))');
        
        % logical array of whether predicted label was same as truth label
        fold_correct = (fold_preds==y_train(X_partitions.test(fold_num)));
        
        % get the confusion matrix
        conf_mat = confusionmat(y_train(X_partitions.test(fold_num)), fold_preds);
        
        % derive precision and recall from confusion matrix
        precision = conf_mat(2,2) / (conf_mat(2,2) + conf_mat(1,2));
        recall = conf_mat(2,2) / (conf_mat(2,2) + conf_mat(2,1));
        
        % get f1 score from precision and recall
        f1_score = (2*precision*recall) / (precision + recall);
        
        % add current ccr, precision, recall, f1 to respective folds array
        folds_ccrs(fold_num, 1) = sum(fold_correct)/numel(fold_correct);
        folds_precisions(fold_num, 1) = precision;
        folds_recalls(fold_num, 1) = recall;
        folds_f1s(fold_num, 1) = f1_score;
        
    end
    
    % take average from 5 folds, save it as value for current c
    ccrs_for_c(c_idx, 1) = mean(folds_ccrs);
    precisions_for_c(c_idx, 1) = mean(folds_precisions);
    recalls_for_c(c_idx, 1) = mean(folds_recalls);
    f1s_for_c(c_idx, 1) = mean(folds_f1s);
    
end

% save variables so that won't have to do time consuming calculations
save('svm_cv_stats.mat', 'ccrs_for_c', 'precisions_for_c', 'recalls_for_c', 'f1s_for_c');


%% PLOT CV - CCR

x = log2(c_vals);

plot(x, ccrs_for_c);
hold on;
plot(x, precisions_for_c);
hold on;
plot(x, recalls_for_c);
hold on;
plot(x, f1s_for_c);
legend('CCR', 'Precision', 'Recall', 'F-score');

% set histogram properties
ax = gca;
ax.Title.String = 'Regularization parameter vs CCR, Precision, Recall, F-score';
ax.XLabel.String = 'Value of C (log scale, base 2)';
ax.YLabel.String = 'Value of CCR, Precision, Recall, F-score';


%% OPTIMAL VALUE OF C

% index at which max ccr occurs
[~, best_ccr_c_idx] = max(ccrs_for_c);

% index at which max recall occurs
[~, best_recall_c_idx] = max(recalls_for_c);

% index at which max f1 score occurs
[~, best_f1_c_idx] = max(f1s_for_c);

% value of c for which max ccr occurs
best_ccr_c = c_vals(best_ccr_c_idx);

% value of c for which max recall occurs
best_recall_c = c_vals(best_recall_c_idx);

% value of c for which max f1 score occurs
best_f1_c = c_vals(best_f1_c_idx);


%% TRAIN ON ALL TRAIN DATA AND TEST ON ALL TEST DATA

% train svm on current training set
svm_struct = svmtrain(words_in_train_docs', y_train, ...
                    'autoscale', 'false', ...
                    'boxconstraint', best_f1_c*ones(numel(y_train,1),1), ...
                    'kernel_function', 'linear');
                
% classifier predictions 
preds = svmclassify(svm_struct, words_in_test_docs');

% logical array of whether predicted label was same as truth label
correct = (preds==y_test);

% get ccr
ccr = sum(correct)/numel(correct);

% get confusion matrix
conf_mat = confusionmat(y_test, preds);

disp(ccr);

toc