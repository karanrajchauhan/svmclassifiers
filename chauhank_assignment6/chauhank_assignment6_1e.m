
% Format is DocID, WordID, WordCount

%% LOAD DATA

% directory where all the data is stored
data_dir = './files_for_assignment6/';

% load train data
fileID = fopen(strcat(data_dir, 'train.data'));
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

% load test data
fileID = fopen(strcat(data_dir, 'test.data'));
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

% load train labels
fileID = fopen(strcat(data_dir, 'train.label'));
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

% load test labels
fileID = fopen(strcat(data_dir, 'test.label'));
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

% load vocab
vocab = importdata(strcat(data_dir, 'vocabulary.txt'));

% load stopwords
stopwords = importdata(strcat(data_dir, 'stoplist.txt'));


%% REMOVE STOP WORDS

% get word id corresponding to each stop word
[~, stop_word_ids] = ismember(stopwords, vocab);

% do not consider words that are in stoplist but not vocab
stop_word_ids = unique(stop_word_ids(stop_word_ids~=0));

% remove stop words from train data
keep_idx = ~ismember(X_train(:,2), stop_word_ids);
X_train = X_train(keep_idx, :);

% remove stop words from test data
keep_idx = ~ismember(X_test(:,2), stop_word_ids);
X_test = X_test(keep_idx, :);


%% CREATE "WORD-COUNT IN DOCUMENT" MATRIX

% initialize for use inside for loops
doc_indices = zeros(size(X_train, 1), 1);

% jth col of this matrix is word count vector for doc id j
words_in_train_docs = zeros(numel(vocab), numel(y_train));

% create the matrix for train data
for doc_id=1:numel(y_train)
    
    % indices where doc id is same is current doc_id
    doc_indices = X_train(:,1) == doc_id;
    
    % populate the matrix with the data for current doc_id
    words_in_train_docs(X_train(doc_indices, 2), doc_id) = X_train(doc_indices, 3);

end

% jth col of this matrix is word count vector for doc id j
words_in_test_docs = zeros(numel(vocab), numel(y_test));

% create the matrix for train data
for doc_id=1:numel(y_test)
    
    % indices where doc id is same is current doc_id
    doc_indices = X_test(:,1) == doc_id;
    
    % populate the matrix with the data for current doc_id
    words_in_test_docs(X_test(doc_indices, 2), doc_id) = X_test(doc_indices, 3);

end

% tranpose as per the needs of svmtrain function to be used
words_in_train_docs = words_in_train_docs';
words_in_test_docs = words_in_test_docs';


%% CREATE SVM STRUCT OBJECTS AND STORE IN VECTOR

% maps linear index to class a - class b
linIdx2ClassesMap = repmat([0 0], 190, 1);
idx = 0;
for i = 1:20
    for j = i+1:20
        idx = idx + 1;
        linIdx2ClassesMap(idx, :) = [i j];
    end
end

% initialize matrix of structs
svm_structs = repmat(struct('SupportVectors', [], 'Alpha', [], 'Bias', [], 'KernelFunction', [], 'KernelFunctionArgs', [], 'GroupNames', [], 'SupportVectorIndices', [], 'ScaleData', [], 'FigureHandles', []),...
                    1, 190);

% initialize 'X for current svm' indexer
curr_svm_X_idx = false(size(words_in_train_docs, 1), 1);

svm_idx = 0;
tic
for class_a = 1:20
    
    for class_b = 1+class_a:20
        
        svm_idx = svm_idx + 1;
        
        % indices of doc-ids that belong to either class a or b 
        curr_svm_X_idx = y_train==class_a | y_train==class_b;
        
        % get current svm
        curr_svm_struct = svmtrain(words_in_train_docs(curr_svm_X_idx, :),...
                                    y_train(curr_svm_X_idx), ...
                                    'autoscale', 'false', ...
                                    'kernel_function', 'linear');
        % save the svm
        svm_structs(svm_idx) = curr_svm_struct;

    end
    
end
toc

%% TEST ON TEST DATA

% matrix with predictions for each point by each classifier
pts_svms_preds = zeros(numel(y_test), 20);

svm_idx = 0;
tic
for class_a = 1:20
    
    for class_b = 1+class_a:20
        
        % column number of classifier
        svm_idx = svm_idx + 1;
        
        % predictions made by classifier
        curr_svm_preds = svmclassify(svm_structs(svm_idx), words_in_test_docs);
        
        pts_svms_preds(:, svm_idx) = curr_svm_preds;

    end
    
end
toc

%% MAJORITY VOTE ON PREDICTIONS

% overall predictions
preds = mode(pts_svms_preds,2);

% overall ccr
ccr = sum(preds==y_test)/numel(y_test);

% confusion matrix for all classes
conf_mat = confusionmat(y_test, int32(preds));
